# CS371p: Object-Oriented Programming Collatz Repo

* Name: Nirav Lalsinghani

* EID: NVL225

* GitLab ID: nlalsinghani

* HackerRank ID: nirav_lalsingha2

* Git SHA: 2ce30c4b3dfc13f2e8dc791f3100918ed338e4c4

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: 6 hours

* Actual completion time: 15 hours

* Comments: Did not push my 200 acceptance test to the public repo because I created them late. I also was not sure what to do with doxygen. I ended up just adding into the pipeline.
