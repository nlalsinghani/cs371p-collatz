// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"
//creates cache of size a million
#define CACHE 1000000
int cache[CACHE];
using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    int tempi = i;
    int tempj = j;
    if(i > j){
        int t = i;
        i = j;
        j = t;
    }
    int max = 0;
    //Opt from class that makes us not compute things that aren't necessary. Example talked about in class 10 - 100 vs 50 - 100
    if(i < j >> 1){
        i = j >> 1;
    }
    for(int n = i; n <= j; n++){
        long x = n;
        int count = 1;
            while(x > 1){
                if((x % 2) == 0){
                    x = x / 2;
                }
                else{
                    x = (3 * x) + 1;
                }
                //Intermediate caching, as talked about in class, doesn't just store the numbers between i and j but also 
                //numbers that go outside the range when mult by 3. Finds those outside the range and adds it to current count. 
                if(x >= 0 && x < 1000000){
                    if(cache[x] != 0){
                        count = cache[x] + count;
                        break;
                    }
                }
                count = count + 1;
            }
            //Stores the calcuated count in cache since it wasn't there already
            cache[n] = count;
        if(count > max){
            max = count;
        }
    }
    return make_tuple(tempi, tempj, max);}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}
