// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));}

TEST(CollatzFixture, read1) {
    ASSERT_EQ(collatz_read("1 1\n"), make_pair(1, 1));}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));}

TEST(CollatzFixture, eval5){
    ASSERT_EQ(collatz_eval(make_pair(999999, 999999)), make_tuple(999999, 999999, 259));}

TEST(CollatzFixture, eval6){
    ASSERT_EQ(collatz_eval(make_pair(1, 3)), make_tuple(1, 3, 8));
}

TEST(CollatzFixture, eval7){
    ASSERT_EQ(collatz_eval(make_pair(500, 1000)), make_tuple(500, 1000, 179));
}

TEST(CollatzFixture, eval8){
    ASSERT_EQ(collatz_eval(make_pair(35408, 63242)), make_tuple(35408, 63242, 340));
}

TEST(CollatzFixture, eval9){
    ASSERT_EQ(collatz_eval(make_pair(281, 610)), make_tuple(281, 610, 144));
}

TEST(CollatzFixture, eval10){
    ASSERT_EQ(collatz_eval(make_pair(610, 982)), make_tuple(610, 982, 179));
}

TEST(CollatzFixture, eval11){
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval13){
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}

TEST(CollatzFixture, eval14){
    ASSERT_EQ(collatz_eval(make_pair(999999, 999998)), make_tuple(999999, 999998, 259));
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");}

TEST(CollatzFixture, print1) {
    ostringstream sout;
    collatz_print(sout, make_tuple(900, 1000, 174));
    ASSERT_EQ(sout.str(), "900 1000 174\n");}
// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");}

TEST(CollatzFixture, solve1) {
    istringstream sin("1 3\n500 1000\n35408 63242\n281 610\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 3 8\n500 1000 179\n35408 63242 340\n281 610 144\n");}
